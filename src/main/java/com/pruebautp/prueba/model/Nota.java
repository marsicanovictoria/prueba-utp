package com.pruebautp.prueba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notas")
public class Nota {

	private long id_nota;
	private long id_usuario;
	private String nota;
	private String curso;
	private long id_curso;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId_nota() {
		return id_nota;
	}
	public void setId_nota(long id_nota) {
		this.id_nota = id_nota;
	}
	@Column(name = "id_usuario", nullable = false)
	public long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	@Column(name = "nota", nullable = false)
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	@Column(name = "curso", nullable = false)
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	@Column(name = "id_curso", nullable = false)
	public long getId_curso() {
		return id_curso;
	}
	public void setId_curso(long id_curso) {
		this.id_curso = id_curso;
	}
	@Override
	public String toString() {
		return "Nota [nota=" + nota + ", id_usuario=" + id_usuario + ", curso=" +curso+ "]";
	}
}
