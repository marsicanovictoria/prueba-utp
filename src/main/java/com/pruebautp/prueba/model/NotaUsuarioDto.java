package com.pruebautp.prueba.model;

public class NotaUsuarioDto {
private String curso;
private Long nota;

public NotaUsuarioDto(String curso, String usuario, Long nota) {
	
	this.curso = curso;
	this.nota = nota;
}


@Override
public String toString() {
	return "NotaUsuarioDto [nota=" + nota + ", curso=" + curso + "]";
}
}
