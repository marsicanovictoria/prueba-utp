package com.pruebautp.prueba.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebautp.prueba.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}
