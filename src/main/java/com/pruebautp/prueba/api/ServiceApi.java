package com.pruebautp.prueba.api;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pruebautp.prueba.model.Nota;
import com.pruebautp.prueba.repository.NotaRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")

public class ServiceApi {
	@Autowired
	private NotaRepository notaRepository;
		
	@GetMapping("/notas/{id}")
	public ResponseEntity<Nota> getEmployeeById(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException {
		System.out.println("*****controller ");
		Nota nota = notaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));
		return ResponseEntity.ok().body(nota);
	}

	
	@PostMapping("notas")
	@ResponseBody
	public Nota cargarNota(@RequestBody Nota nota){
		return notaRepository.save(nota);
	}
}
