package com.pruebautp.prueba.request;

public class RequestNota {
	
	private Long nota;
	private String curso;
	public Long getNota() {
		return nota;
	}
	public void setNota(Long nota) {
		this.nota = nota;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}

}
