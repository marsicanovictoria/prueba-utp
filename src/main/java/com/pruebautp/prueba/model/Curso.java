package com.pruebautp.prueba.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "curso")
public class Curso {

	private long id_curso;
	private String curso;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId_curso() {
		return id_curso;
	}
	public void setId_curso(long id_curso) {
		this.id_curso = id_curso;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
}
