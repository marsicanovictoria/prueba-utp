package com.pruebautp.prueba.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebautp.prueba.model.Nota;

public interface NotaRepository extends JpaRepository<Nota, Long>{

}
